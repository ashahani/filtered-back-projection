/*=================================================================
 *
 * smearing.c
 * 
 * This .mex file performs the linear interpolation versions of 
 * the backprojection in backProject.m.
 * 
 * Each loop can be replaced with one of
 *
 *         img = smearing(p, costheta, sintheta, N, offset)
 *
 * The input parameters are:
 *
 *         p          Radon transform, with projections stored in columns
 *                    (with filtering in the Fourier domain)
 *         costheta   a vector (column or row) holding the cosine of the
 *                    projections angles
 *         sintheta   a vector (column or row) holding the sine of the
 *                    projections angles
 *         N          the output image with be NxN (this is a double float)
 *         offset     sample-detector offset correction (typically +/- 1..10)
 *
 * The output is:
 *
 *         img        an NxN (double float) matrix
 *
 * Written by Jeff Orchard (jorchard@cs.uwaterloo.ca, November 1, 2006), 
 * and adapted by Ashwin Shahani (shahani@u.northwestern.edu, February 28, 2013).
 * This version of Jeff's code includes offset correction for samples that are
 * not aligned exactly with the detector, and removes the section of code on
 * nearest-neighbor interpolation. (Linear interpolation is more accurate.)
 *
 *=================================================================*/

#include <math.h>
#include "mex.h"



void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{
	/* INPUT PARAMETERS */
    double *p;			/* filtered projections (one per col) */
	double *costheta;	/* pre-computed cos of proj angles */
	double *sintheta;	/* pre-computed sin of proj angles */
	double *Nptr;		/* size of reconstructed image */
	double *offset;	    /* sample-detector offset correction */
	
	/* OUTPUT PARAMETERS */
	double *img;		/* output image */
	
	
	/* Other variables */
	int N;				/* integer copy of Nptr (above) */
	double off;	/* double copy of offset (above) */
	int x_idx, y_idx, theta_idx;	/* loop indicies */
	double x;			/* stores x-coordinate of pixel */
	/*double y;*/		/* stores y-coordinate of pixel (not needed) */
	double ctr, xleft, ytop;	/* used to tranform from matrix indecies */
								/* to (x,y) coords (see iradon.m) */
	int len;		/* length of each projection (spatial dimension) */
	int n_proj;		/* number of projection angles (length of costheta vector, eg) */
	int ctr_idx;	/* centre index for projections */
	
	/* Temporary variable used for code optimization */
	double cos_theta, sin_theta, t;
	int ctr_idx_1;	/* = ctr_idx + 1 (for optimization purposes) */
	double x_cos_theta;	/* = x * cos_theta */
	int a;
	double *proj;	/* points at the start of a projection (a column) */
	double *img_ptr;
	
    
    /* Check for proper number of arguments */
    
    if (nrhs != 5) { 
		mexErrMsgTxt("Five input arguments required."); 
    } else if (nlhs > 1) {
		mexErrMsgTxt("Too many output arguments."); 
    } 
	
    p = mxGetPr(prhs[0]);
    costheta = mxGetPr(prhs[1]);
    sintheta = mxGetPr(prhs[2]);
    Nptr = mxGetPr(prhs[3]);
	offset = mxGetPr(prhs[4]);
	
	N = (int)(Nptr[0]);
	off = (int)(offset[0]);
	len = mxGetM(prhs[0]);
	n_proj = mxGetM(prhs[1]) * mxGetN(prhs[1]); /* use product to account for row- or col-vectors */
	
	/* The line below takes care of the difference between Matlab matrix indexing
	 * (base-1) and C-style matrix indexing (base-0).  The corresponding Matlab code is
	 *       ctr = floor((N + 1)/2);
	 * eg. if N=8
	 *          Matlab -> ctr = index of 4
	 *          C      -> ctr = index of 3
	 */
	ctr = floor((N-1) / 2);
	
	xleft = -ctr;
	ytop = ctr;
	
	ctr_idx = (int)floor(len/2);  /* centre index for projections */
	ctr_idx_1 = ctr_idx + 1;
    
    
    /* Create a matrix for the return argument */
    plhs[0] = mxCreateDoubleMatrix(N, N, mxREAL);
    img = mxGetPr(plhs[0]);  /* Get pointer to the data array */
	
	
    
    for (theta_idx=0 ; theta_idx<n_proj ; theta_idx++)
	{
	
		cos_theta = costheta[theta_idx];
		sin_theta = sintheta[theta_idx];
		proj = (p+theta_idx*len);  /* point at proper column */
		img_ptr = img;
		
		/* The loops below are designed to fly through the elements of the output image (img) in
		 * linear-memory sequence... makes it faster.  The matricies are stored column-by-column.
		 * The switch statement is outside the loops for optimization purposes.
		 */		
			
		
		for (x_idx=0 ; x_idx<N ; x_idx++)
		{
			x = xleft + x_idx;  /* x-coord */
			x_cos_theta = x*cos_theta;  /* perform this multiplication outside the y-loop */
			t = x_cos_theta + ytop*sin_theta + off;  /* After this, t can simply be decremented by sin_theta each y-iter */
			
			for (y_idx=0 ; y_idx<N ; y_idx++)
			{
				/*y = ytop - y_idx;
				/*t = x_cos_theta + y*sin_theta;*/  /* removed for optimization purposes */
				
				a = floor(t);
				
				/* The following line is the slower version (2 multiplications) */
				/**img_ptr++ += (t-a)*proj[a+ctr_idx_1] + (1-t+a)*proj[a+ctr_idx]; */
				
				/* This is the speedy version (1 multiplication) */
				*img_ptr++ += (t-a)*( proj[a+ctr_idx_1] - proj[a+ctr_idx] ) + proj[a+ctr_idx];
				
				t -= sin_theta;  /* decrement t */
			} /* end of y-loop */
		} /* end of x-loop */
			
			/* break; */
			
	} /* end of theta loop */
	

    return;
    
}


