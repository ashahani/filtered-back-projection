function FBP = backProject(sinogramdir, theta, offset)

% backProject: apply Filtered Back Projection (FBP) on sinogram slice.

% For mathematical details, refer to Ch. 3 in Kak and Slaney's
% 'Principles of Computerized Tomographic Imaging' (online).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Running the Filtered Back Projection Algorithm ...\n \n')

sinogram = dlmread(sinogramdir);
[N, K] = size(sinogram);        % K is number of angles, N number of detectors
                                % e.g., for sample AlCu_30_g, N = 13xx, K = 501
theta = theta .* (pi/180);                                
costheta = cos(theta);          % Need these for backprojection step
sintheta = sin(theta);
                                
fft_N = N;
R_fft = fft(sinogram, fft_N);   % Fourier Transform of the Projections

f = -(fft_N/2):1:(fft_N/2 - 1); % Define the frequency domain
cut = (0.6) * (fft_N/2);        % Cut-off frequency for parzen filter

% Define the ramp filter in frequency domain
ramp = (abs(f)./(fft_N/2) .* (f <= 0) + abs(f)./(fft_N/2) .* (f >= 0))';
ramp = fftshift(ramp);

% Define the parzen filter in frequency domain
parzen = ((1 - 6*((abs(f)./cut).^2).*(1-abs(f)./cut)) .* (abs(f) <= (cut/2)) + ... 
    (2*(1-abs(f)./cut).^3) .* (abs(f) > (cut/2) & abs(f) <= cut) + ... 
    0 .* (abs(f) > cut))';
parzen = fftshift(parzen);

% Overall (combined) filter is product of the two filters 
combined = ramp .* parzen;

R_filtered = zeros(fft_N, K);

% Multiply the combined filter and the Fourier Transform of the Projections
for i = 1:K
    R_filtered(:,i) = R_fft(:,i) .* combined;
end

% Inverse Fourier Transform of filtered projections; need only real part
R_ifft = real(ifft(R_filtered));
% figure(1); imshow(R_ifft);

% Apply the actual backprojection step, which loops over theta
% Sending to mex program smearing.c
imLength = 2*floor( size(R_ifft,1)/(2*sqrt(2)));
FBP = smearing(R_ifft, costheta, sintheta, imLength, offset);

% Display the filtered back projection
% close all; figure(1); imshow(FBP, []);
                              % now scale the image intensities by pi/K
FBP = FBP * pi/(2*length(theta)); 

                              % convert intensities in FBP to 0..1 interval
FBP = (FBP - min(min(FBP))) ./ (max(max(FBP)) - min(min(FBP)));

end
