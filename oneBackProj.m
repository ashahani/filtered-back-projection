% Filtered Back Projection for one single cross-section only.

% For mathematical details, refer to Ch. 3 in Kak and Slaney's
% 'Principles of Computerized Tomographic Imaging' (online).

function [R_initial, FBP, FBP_sc] = oneBackProj()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% I N P U T S %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

directoryIn = '~/Desktop/1';
sinoDirect = '~/Desktop/sinogram.txt';
sampDirect = '~/Desktop/sample.tif';

position = 400;             % sinogram 'slice' of interest
cutOffLeft = 73;            % define the edges of sinogram
cutOffRight = 573;

darkStart = 1;              % define darks and flats when finding sinogram
darkEnd = 21;
flatStart = darkEnd + 1;
flatEnd = 72;

theta = 0:0.36:180;         % look at .log file to figure out
offset = -2;                % offset correction for sample-detector 

windLeft = 0.2851;          % Graphics:  use imcontrast(FBP)
windRight = 0.7585;

%%%%%%%%%%%%%%%%%%%%%%%%%%% F U N C T I O N S %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R_initial = beamToSinogram(directoryIn, sinoDirect, position, ...
    cutOffLeft, cutOffRight, darkStart, darkEnd, flatStart, flatEnd);

FBP = backProject(sinoDirect, theta, offset);

FBP_sc = imadjust(FBP, [windLeft; windRight], [0; 1]);
imwrite(FBP_sc, sampDirect, 'tif');

close all;

figure(1); imshow(R_initial, []); title('Original Projection'); axis off;
figure(2); imshow(FBP, []); title('Filtered Back Projection'); axis off;
figure(3); imshow(FBP_sc, []); title('Scaled Filtered Back Projection'); ...
    axis off;

end