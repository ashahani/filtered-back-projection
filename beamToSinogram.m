% gittest

function sinogram = beamToSinogram(directoryIn, sinoDirect, position, ...
    cutOffLeft, cutOffRight, darkStart, darkEnd, flatStart, flatEnd)

% Beam to Sinogram: get the sinogram(s) from the beam data, at one timestep.
% Note 12/12: this only collects one sinogram slice at 'position'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get the '.tif' files corresponding to beam data.

% (Before switching directories, get the correct flat file, if necessary)
% flat = double(imread('~/Documents/MatlabFiles/AlCu_20_d_0020_0022.tiff'));

fprintf('Navigating to appropriate folder ...\n\n')
cd(directoryIn); 

fprintf('Finding projection files ...\n\n')
D = dir('*.tif');                           % Find only .tif files

[row, col] = size(imread(D(1).name));
length = numel(D);

Dstack = zeros(row, col, length);           % 1/13: This step takes too long!
                                            % array is absolutely huge

fprintf('Computing the average dark image ...\n')
dark = imread(D(darkStart).name); 

for i=2:darkEnd
    fprintf('Loading file number %f\n \n', i)

    dark = dark + (imread(D(i).name));   
end

dark = dark ./ darkEnd;                     % 1/13: Take the average dark image.
                                            % Helps remove ring artifact

fprintf('Computing the average flat image ...\n')
flat = imread(D(flatStart).name);

for i = (flatStart+1):flatEnd
    fprintf('Loading file number %f\n \n', i)
    
    flat = flat + (imread(D(i).name));
end
                                            % Take average flat image.
flat = (flat ./ (flatEnd - flatStart + 1)) - dark;

fprintf('Now stacking all projections in 3D array \n \n')

for i=1:length
                                            % 2/13: This also takes ages.
                                            % though Dstack is preallocated
    fprintf('Loading file number %f\n \n', i)
    
    Dstack(:,:,i) = (imread(D(i).name) - dark) ./ flat;
                                            % Note all slices normalized!
end

% Extract the sinogram(s) at value(s) of position.

fprintf('Collecting the sinogram data ...\n \n')

sinogram = reshape(Dstack(position, :, :), col, length);
sinogram = - log(sinogram);                 % Due to Beer's Law
sinogram = sinogram(:,cutOffLeft:cutOffRight);

% fprintf('Plotting one sample sinogram ...\n')
% close all; figure; 
% imshow(sinogram, []); title('Sinogram at x = 400, t = 52'); axis off;

% Write the sinogram(s) to .tif file, to be used in backProject.m

fprintf('Writing the sinogram data to file ...\n')
dlmwrite(sinoDirect, sinogram)

cd('~/Documents/MATLAB/Proj Compilation');